This R script is intended to be run from the root of the repository,
e.g., =/home/matt/git-repos/geog-435-dev= like so:

#+BEGIN_SRC bash
Rscript deploy/deploy.R
#+END_SRC

This helps with weird working directory issues in R where it's hard to
set it to the previous directory based on relative path. Currently,
presentations (.html files) and images (all files in =img/= are copied
over automatically for the =geog-435-dev= repo. Otherwise, it's just
the files specified in =config.yaml=, in the root of the directory
(and project specific, of course).
