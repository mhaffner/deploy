#!/bin/bash

if [ $(basename `pwd`) = "geog-435" ]; then
    title="geog-435 file tree"
elif [ $(basename `pwd`) = "geog-335" ]; then
    title="geog-335 file tree"
elif [ $(basename `pwd`) = "geog-111" ]; then
    title="geog-111 file tree"
fi

# TODO figure out how to hide files associated with shapefiles (that I use
# during testing) without hiding other things in the data directory

# i dont want data to be seen in human geography
if [ $(basename `pwd`) = "geog-111" ]; then
    # the -I flag hides the files from the index, but they are still present in the directory
    tree ./public -H '.' -T "$title" -I "reveal*|img*|style*|index*|content*|data" --noreport --charset utf-8 > ./public/index.html
else
    # the -I flag hides the files from the index, but they are still present in the directory
    tree ./public -H '.' -T "$title" -I "reveal*|img*|style*|index*|content*" --noreport --charset utf-8 > ./public/index.html
fi
