echo -e "\033[0;32mCommiting changes in public...\033[0m"

# add all files
git add public/*

# create commit message
msg="building and deploying new site `date`"

# note sure what the function of this is - maybe build site even if there is
# noting to commit
if [ $# -eq 1 ]; then
    msg="$1"
fi

# commit changes
git commit -m "$msg"

# push
git push origin master

echo -e "\033[0;32mDeploying site to GitLab Pages...\033[0m"
